console.log(`[BetterBrick] Resources script loaded from GitLab.`);

function NewDialog(height, Title, Body, Button1, Button2, Action1, Action2) {
  var Dim = document.createElement("div");
  Dim.style.background = "rgba(0, 0, 0, 0.5)";
  Dim.style.width = "100%";
  Dim.style.height = "100%";
  Dim.style.position = "fixed";
  Dim.style.top = "0";
  Dim.style.left = "0";
  Dim.className = "dialogClass";
  Dim.setAttribute('onclick', `RemoveDialogs()`);
  document.getElementsByTagName("body")[0].appendChild(Dim);

  var Dialog = document.createElement("div");
  Dialog.style.position = "absolute";
  Dialog.style.margin = "auto";
  Dialog.style.top = "0";
  Dialog.style.right = "0";
  Dialog.style.bottom = "0";
  Dialog.style.left = "0";
  Dialog.style.width = "325px";
  Dialog.style.height = height;
  Dialog.style.marginTop = "20%";
  Dialog.style.textAlign = "center";
  Dialog.style.overflow = "hidden";
  Dialog.className = "dialogClass";
  Dialog.id = "box";

  Dialog.innerHTML = `<div id="subsect"><h3>${Title}</h3></div><p>${Body}</p><input type="button" value="${Button1}" onclick="${Action1}" style="margin-right: 5px;"><input type="button" value="${Button2}" onclick="${Action2}">`;
  if (Button1 !== null) {
    Dialog.innerHTML = `<div id="subsect"><h3>${Title}</h3></div><p>${Body}</p><input type="button" value="${Button1}" onclick="${Action1}" style="margin-right: 5px;"><input type="button" value="${Button2}" onclick="${Action2}">`;
  } else {
    Dialog.innerHTML = `<div id="subsect"><h3>${Title}</h3></div><p>${Body}</p>`;
  }

  document.getElementsByTagName("body")[0].appendChild(Dialog);
}

function RemoveDialogs() {
  var Dialogs = $(".dialogClass");
  while (Dialogs.length > 0) {
    Dialogs[0].parentNode.removeChild(Dialogs[0]);
  }
}

function LogoutDialog() {
  NewDialog(
    `108px`,
    `Logging Out`,
    `Are you sure you want to log out?`,
    `Log Out`,
    `Cancel`,
    `window.location = "https://www.brick-hill.com/logout/"`,
    `RemoveDialogs()`
  );
}

function LoginDialog() {
  NewDialog(
    '194px',
    'Log In',
    `<form action="https://www.brick-hill.com/login/" method="POST"><strong>Username</strong><br /><input type="text" name="username" /><br /><br /><strong>Password</strong><br /><input type="password" name="password" /><br /><br /><input type="submit" name="ln" value="Log In" /></form>`
  );
}
